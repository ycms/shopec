<?php

Route::group(['prefix' => 'install', 'namespace' => 'Modules\Install\Http\Controllers'], function () {


    Route::get('/', 'IndexController@index');
    Route::any('{action}', function ($action) {
        //kd($action);
        return App::make('Modules\Install\Http\Controllers\IndexController')->index($action);
    })->where(['action' => '\w+']);
});