<?php namespace Modules\Install\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class IndexController extends Controller {
	
	public function index($action = null)
	{
        //extract($GLOBALS,EXTR_REFS);
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db, $sess;

        require 'install/'.($action ?: 'index').'.inc.php';

		//return view('install::index');

	}
	
}