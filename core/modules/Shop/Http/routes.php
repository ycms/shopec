<?php

error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE & ~E_DEPRECATED);


Route::group(['prefix' => '', 'namespace' => 'Modules\Shop\Http\Controllers'], function()
{
	Route::get('/', 'ShopController@index');
    Route::any('{action}', function ($action) {
        //kd($action);
        return App::make('Modules\Shop\Http\Controllers\ShopController')->index($action);
    })->where(['action' => '(?!(admin|mobile|install|demo|plugins))\w+']);
});