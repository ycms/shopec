<?php namespace Modules\Shop\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class ShopController extends Controller {
	
	public function index($action = 'index')
	{
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db;

        require __DIR__.'/apps/'.($action ?: 'index').'.php';


        //require __DIR__.'/apps/index.php';

		//return view('shop::index');
	}
	
}