<?php namespace Modules\Mobile\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class MobileController extends Controller {
	
	public function index()
	{
		return view('mobile::index');
	}
	
}