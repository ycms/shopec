<?php

Route::group(['prefix' => 'mobile', 'namespace' => 'Modules\Mobile\Http\Controllers'], function()
{
	Route::get('/', 'MobileController@index');
});