<?php

Route::group(['prefix' => 'demo', 'namespace' => 'Modules\Demo\Http\Controllers'], function()
{
	Route::get('/', 'DemoController@index');
});