<?php

Route::group(['prefix' => 'admin', 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
	Route::get('/', 'AdminController@index');
    Route::any('{action}', function ($action) {
        //kd($action);
        return App::make('Modules\Admin\Http\Controllers\AdminController')->index($action);
    })->where(['action' => '\w+']);
});