<?php namespace Modules\Admin\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class AdminController extends Controller {
	
	public function index($action = 'index')
	{
        global $_LANG, $smarty, $err, $ecs, $_CFG, $db, $sess;



        require __DIR__.'/apps/'.($action ?: 'index').'.php';
		//return view('admin::index');
	}
	
}